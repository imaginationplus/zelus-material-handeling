#!/bin/bash

# update composer
echo "Updating Composer..."
composer self-update --quiet

# update WP-CLI
echo "Updating WP-CLI"
wp cli update --yes --allow-root

# run composer install
echo "Running Composer install"
cd /var/www && composer install --quiet

# setup .env file
echo "Set up .env"
cp .env.example .env

# install WordPress
echo "Installing WordPress..."
wp core install --url='http://local.zelus.com' --title='Zelus Material Handeling' --admin_user=hc-admin --admin_password=password --admin_email="cms@imaginationplus.com" --skip-email --allow-root
wp package install aaemnnosttv/wp-cli-dotenv-command --allow-root
wp dotenv salts generate --allow-root
wp theme activate bb-theme-child --allow-root
wp plugin activate --all --allow-root
wp rewrite structure '/%postname%' --allow-root
wp rewrite flush --allow-root
