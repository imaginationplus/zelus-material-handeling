<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

/**
 * Hook for disabling the search functionality
 *
 * @param query string sent by search
 * @param error $request URL the user is coming from.
 */

function disable_search($query, $error = true)
{
  if (is_search() && !is_admin()) {
      $query->is_search = false;
      $query->query_vars[s] = false;
      $query->query[s] = false;
      if ($error == true) $query->is_404 = true;
  }
}
add_action('parse_query', 'disable_search');
add_filter('get_search_form', function(){ 
return null;
});


//Work around to change the gravity form user registration default email content by calling send new registration email custom function
// add_action( 'gform_after_submission_1', 'become_operator_after_submission', 10, 2 );
// function become_operator_after_submission( $entry, $form ){
// 	$user='';
// 	//Get user_id by email
//     $user_email = rgar( $entry, '1' );
//     $plaintext_pass = rgar( $entry, '2' );
// 	$user = get_user_by('email', $user_email);
//     send_new_registration_email($user->ID, $plaintext_pass);
// }

// // A customized version of the Gravity Forms User Registration email. The default version is just a plaintext
// // email, but we're in the future and send HTML emails. We've also added some simple content to the new user email
// function send_new_registration_email( $user_id, $plaintext_pass = '') {

//     $user = get_userdata( $user_id );
// 	$message = '<p>' . sprintf( __('Hi %s'), $user->first_name ) . ',</p>';
// 	$message.= '<p>' . __( 'Welcome to TandoTraining! ') . '</p>';
// 	$message.= '<p>' . __( 'You have successfully registered and have access to all Tando online training modules. To login, please click the following link and enter your username and password. ') . '</p>';
//     $message.= '<p>' .sprintf( __( 'Username: %s' ), $user->user_email ) . '</p>';
//     $message.= '<p>' .sprintf( __( 'Password: %s' ), $plaintext_pass ) . '</p>';

// 	$result = wp_mail( $user->user_email, __( 'Welcome to Tando Training!' ), $message );
// }

// Adjust your form ID
add_filter( 'gform_form_post_get_meta_5', 'add_my_field' );
function add_my_field( $form ) {
 
    // Create a Single Line text field for the team member's name
    $rated = GF_Fields::create( array(
        'type'   => 'text',
        'id'     => 1101, // The Field ID must be unique on the form
        'formId' => $form['id'],
        'label'  => 'Rated capacity',
        'pageNumber'  => 1, // Ensure this is correct
    ));
 
    // Create a repeater for the team members and add the Rated Capacity field to display inside the repeater.
    $ratedCapacity = GF_Fields::create( array(
        'type'             => 'repeater',
        'description'      => 'Add the rated capacity for each crane',
        'id'               => 1111, // The Field ID must be unique on the form
        'formId'           => $form['id'],
        'label'            => 'Crane rated capacity',
        'addButtonText'    => 'Add crane capacity', // Optional
        'removeButtonText' => 'Remove crane capacity', // Optional
        'maxItems'         => 10, // Optional
        'pageNumber'       => 1, // Ensure this is correct
        'fields'           => array( $rated ), // Add the fields here.
    ) );
 
    $form['fields'][] = $ratedCapacity;
 
    return $form;
}
 
// Remove the field before the form is saved. Adjust your form ID
add_filter( 'gform_form_update_meta_5', 'remove_my_field', 5 );
function remove_my_field( $form_meta, $form_id, $meta_name ) {
 
    if ( $meta_name == 'display_meta' ) {
        // Remove the Repeater field: ID 1111
        $form_meta['fields'] = wp_list_filter( $form_meta['fields'], array( 'id' => 1111 ), 'NOT' );
    }
 
    return $form_meta;
}